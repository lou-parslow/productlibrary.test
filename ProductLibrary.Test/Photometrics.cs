using NUnit.Framework;
using System;
using System.IO;

namespace ProductLibrary.Test
{
    public class PhotometricsTests
    {
        [Test]
        public void Usage()
        {
            string db_filename = $"{Dataset.Path}/TestInput/FixtureDB/Products.sqlite";
            Assert.True(File.Exists(db_filename), db_filename);

            if (Environment.GetEnvironmentVariable("MUSCO_PRODUCT_PWD") is string pwd)
            {
                string connectionString = $"Provider=System.Data.SQLite3;Version=3;Read Only=True;Data Source={db_filename};Password={pwd}";
                using (Musco.Design.Aim.ProductLibrary.DB.Photometrics db = new Musco.Design.Aim.ProductLibrary.DB.Photometrics(connectionString))
                {
                    Assert.AreEqual(0, db.GetUniqueIDs().Length, "db.GetUniqueIDs().Length");
                }
            }
            else
            {
                Assert.Fail("MUSCO_PRODUCT_PWD environment variable not found");
            }
        }

        public static Musco.kmarshall.Utils.Data.Dataset Dataset
        {
            get
            {
                if (_dataset != null)
                {
                    return _dataset;
                }

                _dataset = new Musco.kmarshall.Utils.Data.Dataset("productlibrary_test", "0.0", "Test");
                _dataset["revision"] = "8297";
                return _dataset;
            }
        }

        private static Musco.kmarshall.Utils.Data.Dataset _dataset = null;
    }
}