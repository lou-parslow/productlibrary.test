RAYKIT_GLOBALS = true
require "raykit"

task :env do
  start_task :env
  show_value("PROJECT.version", "#{PROJECT.version}")
end

task :build => [:env] do
  start_task :build
  run("dotnet build #{PROJECT.name}.sln --configuration Release")
end

task :test => [:build] do
  start_task :test
  run("dotnet test #{PROJECT.name}.sln")
end

task :tag => [:test] do
  start_task :tag
  if ENV["CI_SERVER"].nil?
    if GIT_DIRECTORY.has_tag PROJECT.version
      puts "git tag #{PROJECT.version} already exists"
    else
      puts "git tag #{PROJECT.version} does not exist"
      if (!PROJECT.read_only?)
        run("git integrate")
        run("git tag #{PROJECT.version} -m\"#{PROJECT.version}\"")
        run("git push --tags")
      end
    end
  else
    puts "CI_SERVER, skipping tag command"
  end
end


#task :publish => [:tag] do
#  start_task :publish
#  if ENV["CI_SERVER"].nil?
#    nuget = PROJECT.get_dev_dir("nuget")
#    package = "#{PROJECT.name}Client/bin/Release/#{PROJECT.name}Client.#{PROJECT.version}.nupkg"
#    if (SECRETS.has_key?("denver_proget"))
      #PROJECT.run("dotnet nuget push #{package} --skip-duplicate --api-key lparslow:#{SECRETS["denver_proget"]} --source http://denver-packages.musco.com/nuget/NuGet/")
#    else
#      puts "denver_proget SECRET not available"
#    end
#  else
#    puts "CI_SERVER, skipping publish command"
#  end
#end

task :default => [:test] do
  puts "  completed #{PROJECT.elapsed}"
end
